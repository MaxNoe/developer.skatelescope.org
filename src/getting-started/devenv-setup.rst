.. _devenv-setup:

Configuring your development environment
****************************************

Python and Tango development
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A completely configured development environment can be set up very easily. This will include
TANGO, PyTANGO, docker and properly configured IDEs.

VSCode(*Recommended*) and PyCharm are two IDEs that can be configured to support python and
PyTANGO development activities. You will find detailed instructions and how-tos at:

.. toctree::
   :maxdepth: 1 

   devenv-setup/tango-devenv-setup
   devenv-setup/vscode/vscode
   devenv-setup/pycharm/pycharm
   deploy-skampi
   integrate-skampi
